import { combineReducers } from 'redux';
import { shoesReducer } from './shoesReducer';
import { modalReducer } from './modalReducer';

export const rootReducer = combineReducers({
  shoesReducer,
  modalReducer,
});
