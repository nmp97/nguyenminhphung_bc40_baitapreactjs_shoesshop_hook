import './App.css';
import ShoeShop from './Component/ShoeShop/ShoeShop';

function App() {
  return (
    <div>
      <ShoeShop />
    </div>
  );
}

export default App;
