import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function ModalDetailProduct({
  showModalDetail,
  handleCloseModalDetail,
  detailProduct,
}) {
  return (
    <div>
      <Modal show={showModalDetail} onHide={handleCloseModalDetail}>
        <Modal.Header closeButton>
          <Modal.Title className="fs-2 fw-bold">Thông tin sản phẩm</Modal.Title>
        </Modal.Header>
        <Modal.Body className="d-flex">
          <img src={detailProduct.image} alt="" style={{ width: '15rem' }} />
          <div className="p-3">
            <h4>{detailProduct.name}</h4>
            <p className="fw-bold">Giá: {detailProduct.price}$</p>
            <p>
              <span>Mô tả: {detailProduct.description}</span>
              <br />
            </p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleCloseModalDetail}>
            Đóng
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
