export default function CartItem({
  cart,
  handleIncreaseQuantity,
  handleDecreaseQuantity,
  handleDeleteItem,
}) {
  return cart.map((item) => {
    return (
      <div className="cartItem row justify-content-between align-items-center" key={item.id}>
        <div className="col-auto">
          <img src={item.image} alt="" style={{ width: 80, height: 80 }} />
        </div>
        <div className="col-auto">
          <span>{item.price * item.quantity}$</span>
        </div>
        <div className="modal-btn col-auto">
          <button>
            <span
              className="fs-5 fw-bold w-100"
              onClick={() => {
                handleDecreaseQuantity(item.id);
              }}
            >
              -
            </span>
          </button>
          <span className="mx-2">{item.quantity}</span>
          <button>
            <span
              onClick={() => {
                handleIncreaseQuantity(item.id);
              }}
            >
              +
            </span>
          </button>
        </div>
        <div className="col-auto">
          <button
            className="btn btn-outline-danger"
            onClick={() => {
              handleDeleteItem(item.id);
            }}
          >
            Xóa
          </button>
        </div>
      </div>
    );
  });
}
