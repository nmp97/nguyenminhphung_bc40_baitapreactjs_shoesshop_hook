import React from 'react';

export default function EmptyCart({ handleCloseModalCart }) {
  return (
    <div className="text-center py-5">
      <p>Chưa có sản phẩm</p>
      <button
        className="btn btn-outline-primary"
        onClick={() => {
          handleCloseModalCart();
        }}
      >
        Tiếp tục mua sắm
      </button>
    </div>
  );
}
