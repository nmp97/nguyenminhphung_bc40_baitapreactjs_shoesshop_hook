export default function FooterCart({ total, handleCloseModalCart }) {
  return (
    <>
      <div className="d-flex justify-content-between align-content-center">
        <span>Tổng Tiền:</span>
        <span>{total}$</span>
      </div>
      <div className="text-end mt-3">
        <button className="mx-2 btn btn-danger" variant="secondary" onClick={handleCloseModalCart}>
          Đóng
        </button>
        <button className="btn btn-primary" variant="primary">
          Đặt hàng
        </button>
      </div>
    </>
  );
}
