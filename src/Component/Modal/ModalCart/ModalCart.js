import React from 'react';
import Modal from 'react-bootstrap/Modal';
import CartItem from './CartItem';
import EmptyCart from './EmptyCart';
import FooterCart from './FooterCart';
import { ModalBody } from 'react-bootstrap';

export default function ModalCart({
  cart,
  total,
  showModalCart,
  handleCloseModalCart,
  handleIncreaseQuantity,
  handleDecreaseQuantity,
  handleDeleteItem,
}) {
  return (
    <>
      <Modal show={showModalCart} onHide={handleCloseModalCart}>
        <Modal.Header closeButton>
          <Modal.Title>Giỏ Hàng Của Bạn</Modal.Title>
        </Modal.Header>
        {cart.length === 0 ? (
          <ModalBody>
            <EmptyCart handleCloseModalCart={handleCloseModalCart} />
          </ModalBody>
        ) : (
          <>
            <Modal.Body>
              <CartItem
                cart={cart}
                handleIncreaseQuantity={handleIncreaseQuantity}
                handleDecreaseQuantity={handleDecreaseQuantity}
                handleDeleteItem={handleDeleteItem}
              />
            </Modal.Body>
            <Modal.Footer className="d-block">
              <FooterCart total={total} handleCloseModalCart={handleCloseModalCart} />
            </Modal.Footer>
          </>
        )}
      </Modal>
    </>
  );
}
