import React, { memo } from 'react';
import { BagCheck } from 'react-bootstrap-icons';

function Header({ handleOpenModalCart, quantity }) {
  return (
    <div className="sticky-top w-100 z-3 d-flex justify-content-between align-items-center py-2 bg-danger-subtle px-5">
      <h1 className="display-6 fw-bold text-center">NMP SHOES</h1>
      <div
        className="cart d-flex align-items-center p-2 rounded position-relative"
        onClick={() => {
          handleOpenModalCart();
        }}
      >
        <BagCheck className="fs-5" />
        <span className="fs-6 lh-sm px-1">
          Giỏ
          <br />
          Hàng
        </span>
        <span className="cart-quantity">{quantity}</span>
      </div>
    </div>
  );
}

export default memo(Header);
