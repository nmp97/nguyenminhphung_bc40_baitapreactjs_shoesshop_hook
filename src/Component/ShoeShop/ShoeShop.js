import { useState } from 'react';
import ListShoes from './ListShoe';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { ToastContainer } from 'react-toastify';
import { dataShoe } from '../../data/dataShoe';
import ModalDetailProduct from '../Modal/ModalDetailProduct/ModalDetailProduct';
import ModalCart from '../Modal/ModalCart/ModalCart';

let initialValue = { dataShoe };

export default function ShoeShop() {
  let [showModalDetail, setShowModalDetail] = useState(false);
  let [showModalCart, setShowModalCart] = useState(false);
  let [detailProduct, setDetailProduct] = useState({});
  let [cart, setCart] = useState([]);
  let [total, setTotal] = useState(0);
  let [quantity, setQuantity] = useState(0);

  const handleCloseModalDetail = () => {
    setShowModalDetail(false);
  };
  const handleShowModalDetail = (shoe) => {
    setShowModalDetail(true);
    setDetailProduct(shoe);
  };
  const handleCloseModalCart = () => {
    setShowModalCart(false);
  };
  const handleOpenModalCart = () => {
    setShowModalCart(true);
  };
  const handleAddToCart = (shoe) => {
    let cloneCart = [...cart];

    let index = cloneCart.findIndex((item) => item.id === shoe.id);

    if (index === -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
      setQuantity((quantity += 1));
      setTotal((total += newShoe.price));
    } else {
      cloneCart[index].quantity += 1;
      setQuantity((quantity += 1));
      setTotal((total += cloneCart[index].price));
    }

    setCart(cloneCart);
  };
  const handleIncreaseQuantity = (id) => {
    let newCart = [...cart];
    let index = newCart.findIndex((item) => item.id === id);
    newCart[index].quantity += 1;
    setQuantity((quantity += 1));
    setTotal((total += newCart[index].price));
    setCart(newCart);
  };
  const handleDecreaseQuantity = (id) => {
    let newCart = [...cart];
    let index = newCart.findIndex((item) => item.id === id);
    let quantityItem = newCart[index].quantity;

    if (quantityItem > 1) {
      newCart[index].quantity -= 1;
      setQuantity((quantity -= 1));
      setTotal((total -= newCart[index].price));
      setCart(newCart);
    } else {
      newCart[index].quantity -= 1;
      setQuantity((quantity -= 1));
      setTotal((total -= newCart[index].price));
      newCart.splice(index, 1);
      setCart(newCart);
    }
  };
  const handleDeleteItem = (id) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => item.id === id);
    setQuantity((quantity -= cloneCart[index].quantity));
    setTotal((total -= cloneCart[index].quantity * cloneCart[index].price));
    cloneCart.splice(index, 1);
    setCart(cloneCart);
  };

  return (
    <div>
      <div className="content">
        <Header handleOpenModalCart={handleOpenModalCart} quantity={quantity} />

        <ModalCart
          cart={cart}
          showModalCart={showModalCart}
          handleCloseModalCart={handleCloseModalCart}
          total={total}
          handleIncreaseQuantity={handleIncreaseQuantity}
          handleDecreaseQuantity={handleDecreaseQuantity}
          handleDeleteItem={handleDeleteItem}
        />

        <ModalDetailProduct
          showModalDetail={showModalDetail}
          handleCloseModalDetail={handleCloseModalDetail}
          detailProduct={detailProduct}
        />

        <ListShoes
          list={initialValue.dataShoe}
          handleShowModalDetail={handleShowModalDetail}
          handleAddToCart={handleAddToCart}
        />

        <Footer />

        <ToastContainer limit={3} style={{ width: 'auto' }} />
      </div>
    </div>
  );
}
