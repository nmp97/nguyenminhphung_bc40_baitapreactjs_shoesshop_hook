import React from 'react';
import ItemShoe from './ItemShoe';

export default function ListShoe({ list, handleShowModalDetail, handleAddToCart }) {
  return (
    <div>
      <div className="list-shoe container">
        <div className="row g-3">
          {list.map((item) => (
            <ItemShoe
              key={item.id}
              item={item}
              handleShowModalDetail={handleShowModalDetail}
              handleAddToCart={handleAddToCart}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
