import React from 'react';

export default function ItemShoe({ item, handleShowModalDetail, handleAddToCart }) {
  return (
    <div className="col-3">
      <div className="card">
        <img src={item.image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h4 className="card-title">{item.name}</h4>
          <p className="card-text fs-4">Giá: {item.price}</p>
          <div className="d-flex justify-content-between">
            <button
              onClick={() => {
                handleShowModalDetail(item);
              }}
              className="btn btn-warning"
            >
              Xem chi tiết
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                handleAddToCart(item);
              }}
            >
              Mua hàng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
